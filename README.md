# NOIRLab VO Registry

![Latest Release](https://gitlab.com/nsf-noirlab/csdc/vo-services/noirlab-vo-registry/-/badges/release.svg?order_by=release_at "Latest Release")
![Pipeline Status](https://gitlab.com/nsf-noirlab/csdc/vo-services/noirlab-vo-registry/badges/noirlab/pipeline.svg "Pipeline Status")

This is the primary NOIRLab fork of the [VO Registry Template](https://gitlab.com/nsf-noirlab/csdc/vo-services/templates/vo-registry).

[VO Registries](https://ivoa.net/documents/RegistryInterface/20180723/REC-RegistryInterface-1.1.html)
are used by VO applications to discover data and services. This registry implementation is a *publishing registry*, meaning it only
has the features required by the
[OAI-PMH spec](https://www.openarchives.org/OAI/2.0/openarchivesprotocol.htm).
Other VO registries (such as the [RofR](http://rofr.ivoa.net/))
will scrape this registry and then provide additional
search capabilities for discovery.

## Application

The application is defined in `/registry`.
See the [README](registry/README.md) there for application details.

## Collectors

Various resource generation scripts are defined in `/collectors`.
See the [README](collectors/README.md) for more information.

## Infrastructure

Infrastructure is defined in `/terraform`.
See the [README](terraform/README.md) there for details.

## QuickStart

If you don't want to use Terraform or GCP, you can ignore those steps.
The Terraform CI jobs are allowed to fail. The docker container is built
and pushed to the GitLab container registry in the build CI job.

1. Fork the repository
2. Create a branch that will be your specific deployment e.g. "noirlab"
3. Set that branch to be the default branch Settings > Repository > Branch Defaults.
4. Protect that branch: Settings > Repository > Protected Branches
5. Set the Target Project for MRs to your fork: Settings > Merge requests > Target Project > "This project"
This makes MRs default into your fork instead of on the upstream repo.
6. Update terraform (see Terraform README)
7. Deploy the infrastructure
8. Configure the Registry application  
  a. Update pyproject.toml
9. Deploy the application

## Troubleshooting

### Terraform plan GitLab CI job fails the first time it's run

If you're seeing `Invalid value for \"audience\"`, the CI variables probably
aren't set and available. You can add a `- echo $WI_POOL_PROVIDER` to the
`terraform plan` CI job, if it prints `$WI_POOL_PROVIDER` instead of
`[masked]` or the value, then you either have something spelled wrong,
or you might need to check "Expand variable reference" in the CI Variable
window.
