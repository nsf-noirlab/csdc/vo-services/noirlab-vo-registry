"""
Loads production SIA service metadata from discovery endpoint and generates the
appropriate VO Resource files for the registry.
"""
import os
import argparse
import glob
import re
from datetime import (
    datetime,
    timezone
)
import requests
from jinja2 import (
    Environment,
    PackageLoader
)

# Global settings

# The endpoint of the SIA discovery service
SIA_URL = os.environ.get('SIA_DISCOVERY_ENDPOINT')
# Various XML constraints to apply
SHORT_NAME_MAX = 16
# Namespace for this collector (needs to match files)
NAMESPACE="datalab-sia"
# Toggle resource refreshing, used when you want to force an update to all
# resource files
REFRESH_RESOURCES = os.environ.get('REFRESH_RESOURCES') == "True"
# the current timestamp to change resource "updated" field
TIMESTAMP=datetime.now(timezone.utc).strftime('%Y-%m-%dT%H:%M:%SZ')
# Initialize a template environment and our SIA resource template
jinja = Environment(loader=PackageLoader("res_gen"))
sia_template = jinja.get_template("vo-resource.jinja.xml")

# Main methods

def  get_existing_resources(resource_dir, namespace):
    """
    For the given directory return all existing resources for this collector
    """
    existing_files = []
    for file in glob.glob(resource_dir):
        existing_files.append(file)

    # find all existing resource files and store the slugs and file name
    existing_resources = {}
    for file_loc in existing_files:
        match = re.search(rf"{namespace}-(.+)\.xml$", file_loc)
        slug = match.group(1) if match else ""
        file_name = match.group(0) if match else ""
        existing_resources.update({
            slug: {
                "name": slug,
                "file_name": file_name,
                "file_loc": file_loc
            }
        })
    return existing_resources

def generate_files(out_loc):
    """
    The primary method for generating new SIA resources files at the given
    output location
    """
    # load metadata for the current (pre-generation) directory
    existing_resources = get_existing_resources(
        resource_dir=f"{out_loc}/{NAMESPACE}-*.xml",
        namespace=NAMESPACE
    )

    # load the data from the discovery service
    current_services = requests.get(SIA_URL, timeout=5000).json()

    # generate one resource for each service
    for service in current_services:
        name = service.get('name')
        if existing_resources.get(name) is not None and not REFRESH_RESOURCES:
            print(f"{name} - Resource already exists")
            continue
        tmpl_str = sia_template.render(
            **service,
            shortName=name[:SHORT_NAME_MAX],
            updated=TIMESTAMP
            )
        file_name = f"{NAMESPACE}-{name}.xml"
        file_loc = f"{out_loc}/{file_name}"
        with open(file_loc, "w", encoding='utf-8') as resource_file:
            resource_file.write(tmpl_str)
            print(f"Generated: {file_loc}")

    # Mark the status as "deleted" for any services that are no longer in the
    # discovery endpoint.
    for resource in existing_resources.values():
        service_record = next(( s for s in current_services
                               if s.get('name') == resource.get('name')), False)
        # if we have a service for this then we don't need to do anything else
        if service_record is not False:
            continue
        # if we do not have a service record then we should update the existing
        # resource file. First get the current file contents.
        f = open(resource.get('file_loc'), 'r' , encoding='utf-8')
        filedata = f.read()
        f.close()
        # Replace status="active" with status="deleted" and bump the "updated"
        # timestamp in the file
        deleted_res = filedata.replace('status="active"', 'status="deleted"')
        deleted_res = re.sub(r'updated=\"[^\"]+', f"updated=\"{TIMESTAMP}",
                             deleted_res)
        # save the updated file contents
        f = open(resource.get('file_loc'), 'w', encoding='utf-8')
        f.write(deleted_res)
        f.close()
        print(f"{resource.get('name')} - Marked as deleted.")

if __name__ == "__main__":
    parser = argparse.ArgumentParser("sia-resource-gen")
    parser.add_argument("out_dir", help="The directory to output records to.", type=str)
    args = parser.parse_args()

    generate_files(args.out_dir)
