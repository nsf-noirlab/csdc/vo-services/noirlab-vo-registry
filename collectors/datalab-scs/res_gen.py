"""
Loads production SCS service metadata from services endpoint and generates the
appropriate VO Resource files for the registry.
"""
import os
import argparse
import glob
import re
import json
from datetime import (
    datetime,
    timezone
)

import requests
from jinja2 import (
    Environment,
    PackageLoader
)

# Global settings

# The endpoint of the Data Lab services discovery
DL_SCS_DISCOVERY_BASE = os.environ.get('DL_SCS_DISCOVERY_BASE')
# Various XML constraints to apply
SHORT_NAME_MAX = 16
# Namespace for this collector (needs to match files)
NAMESPACE="datalab-scs"
# Toggle resource refreshing, used when you want to force an update to all
# resource files
REFRESH_RESOURCES = os.environ.get('REFRESH_RESOURCES') == "True"
# the current timestamp to change resource "updated" field
TIMESTAMP=datetime.now(timezone.utc).strftime('%Y-%m-%dT%H:%M:%SZ')
# Initialize a template environment and our SIA resource template
jinja = Environment(loader=PackageLoader("res_gen"))
scs_template = jinja.get_template("vo-resource.jinja.xml")

# Main methods

def  get_existing_resources(resource_dir, namespace):
    """
    For the given directory return all existing resources for this collector
    """
    existing_files = []
    for file in glob.glob(resource_dir):
        existing_files.append(file)

    # find all existing resource files and store the slugs and file name
    existing_resources = {}
    for file_loc in existing_files:
        match = re.search(rf"{namespace}-(.+)\.xml$", file_loc)
        slug = match.group(1) if match else ""
        file_name = match.group(0) if match else ""
        existing_resources.update({
            slug: {
                "name": slug,
                "file_name": file_name,
                "file_loc": file_loc
            }
        })
    return existing_resources

def get_datalab_services():
    """
    Load and parse Data Lab cone search services from the services endpoint
    """
    current_services = []
    skipped_services = []
    services_text_url = f"{DL_SCS_DISCOVERY_BASE}?type=scs"
    services_text = requests.get(services_text_url, timeout=5000).text
    for line in services_text.split("\n")[3:]:
        match = re.search(r"^[^\w]*([^\s]+)\s{3}([^\s]+)\s{3}(.*)$", line)
        name = match.group(1) if match else ""
        desc = match.group(3) if match else ""
        if name == "":
            print(f"Unable to parse: {line}")
            continue
        # remove the slash that comes back in the service name and use a dash
        slug = name.replace("/", "-")
        label = name.replace("_", " ").replace("/", " ")
        access_url = f"{DL_SCS_DISCOVERY_BASE}?type=scs&name={slug}&mode=dict"
        external_url = requests.get(access_url, timeout=5000).text
        if 'http' not in external_url:
            #TODO: we should check what to do with these services that do not
            # have a URL. For now we are skipping them
            print(f"No external URL for {slug}")
            skipped_services.append({
                'name': name,
                'reason': 'No external URL found'
            })
            continue
        service = dict(
            name=slug,
            label=label,
            shortName=slug[:SHORT_NAME_MAX],
            description=f": {desc}" if desc else "",
            external_url=external_url
        )
        current_services.append(service)
    return current_services, skipped_services

def generate_files(out_loc):
    """
    The primary method for generating new SCS resource files at the given
    output location
    """
    # load metadata for the current (pre-generation) directory
    existing_resources = get_existing_resources(
        resource_dir=f"{out_loc}/{NAMESPACE}-*.xml",
        namespace=NAMESPACE
    )

    # load the data from the discovery service
    current_services, skipped = get_datalab_services()

    # generate one resource for each service
    for service in current_services:
        name = service.get('name')
        if existing_resources.get(name) is not None and not REFRESH_RESOURCES:
            print(f"{name} - Resource already exists")
            continue
        tmpl_str = scs_template.render(**service, updated=TIMESTAMP)
        file_name = f"{NAMESPACE}-{name}.xml"
        file_loc = f"{out_loc}/{file_name}"
        with open(file_loc, "w", encoding='utf-8') as resource_file:
            resource_file.write(tmpl_str)
            print(f"Generated: {file_loc}")

    # Mark the status as "deleted" for any services that are no longer in the
    # discovery endpoint.
    for resource in existing_resources.values():
        service_record = next(( s for s in current_services
                               if s.get('name') == resource.get('name')), False)
        # if we have a service for this then we don't need to do anything else
        if service_record is not False:
            continue
        # if we do not have a service record then we should update the existing
        # resource file. First get the current file contents.
        f = open(resource.get('file_loc'), 'r' , encoding='utf-8')
        filedata = f.read()
        f.close()
        # Replace status="active" with status="deleted" and bump the "updated"
        # timestamp in the file
        deleted_res = filedata.replace('status="active"', 'status="deleted"')
        deleted_res = re.sub(r'updated=\"[^\"]+', f"updated=\"{TIMESTAMP}",
                             deleted_res)
        # save the updated file contents
        f = open(resource.get('file_loc'), 'w', encoding='utf-8')
        f.write(deleted_res)
        f.close()
        print(f"{resource.get('name')} - Marked as deleted.")

    # show any skipped services in the log
    print(json.dumps(skipped, indent=4))

if __name__ == "__main__":
    parser = argparse.ArgumentParser("scs-resource-gen")
    parser.add_argument("out_dir", help="The directory to output records to.", type=str)
    args = parser.parse_args()

    generate_files(args.out_dir)
