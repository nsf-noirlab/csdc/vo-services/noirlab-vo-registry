# Contributing

(We need a standard contributing document)

- Use Branches or Forks, you can't push to main
- Use Merge Requests
- Run the linter and tests
