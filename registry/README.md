# VO Registry

The VO Registry is FastAPI Python service that provides the necessary features
to run a publishing registry via the OAI-PMH protocol. For more information
about VO publishing registries see the [main README file](../README.md).

## System Requirements

| Name          | Required Version | Details                                       |
|---------------|------------------|-----------------------------------------------|
| Docker Engine | `27+`            | Required for all environments                 |
| Make          | `4.3+`           | **Note:** only required for local development |

## Environment Variables

| Environment       | Variable                   | Description                                                               | Example                  |
|-------------------|----------------------------|---------------------------------------------------------------------------|--------------------------|
| `./registry/.env` | `BASE_URL`                 | The external url of the service.                                          | `https://vo.noirlab.edu` |
|                   | `PORT`                     | The port of the API service                                               | `8080`                   |
|                   | `PUBLISH_PORT`             | Port to publish the container. Used in development shortcut scripts only. | `8080`                   |
| `Gitlab CI`       | `SERVICE_ACCOUNT_EMAIL`    | a terraform output                                                        | ``                       |
|                   | `WI_POOL_PROVIDER`         | a terraform output                                                        | ``                       |
|                   | `TF_VAR_gitlab_project_id` | the gitlab project id (integer)                                           | ``                       |
|                   | `TF_VAR_google_project_id` | the google project id                                                     | ``                       |

## Steps to Run

To run a local copy of this VO Registry:

```
make
```

Then point your browser to `http://localhost:8080` (or the port you defined in `PORT`)

The base OAI-PMH url will be `http://localhost:8080/registry`
so `http://localhost:8080/registry?verb=Identify` will return the Identify xml.

## Testing

To run the unit tests:

```
make test
```

and the end-to-end tests (which require a network connection):

```
make test-e2e
```

and to run the end-to-end tests on a deployed registry:

```
make BASE_URL=https://template.vo.noirlab.edu test-deployed
```

## Configuration

After forking the repository.

- Update config.py with your name and email
- Update static Resource files: authority.xml and registry.xml

## Development

A [Makefile](Makefile) is provided that builds, lints, and tests the registry
application inside the containers defined in the [Dockerfile](Dockerfile).
See the comments in the Makefile.

This repo is configured to use Gitlab CI to build, lint, test, and deploy this
application. See the [gitlab-ci](../.gitlab-ci.yml) file for details.

### Commit Hooks
[Pre-commit](https://pre-commit.com/) can be installed to automate the code
quality and formatting adjustments and the `.pre-commit-config.yaml` file defines
the hooks. It is recommended to activate these during the development setup step to
avoid manual checks and extra commits for formatting. These checks also happen
in the Gitlab CI for those that like to run them manually.

To setup the git hooks, after installing the dev dependencies run:
```
pre-commit install
```

## System Design

This application is a python
[FastAPI](https://fastapi.tiangolo.com/) app that implements
the required components of the
[OAI-PMH specification](http://www.openarchives.org/OAI/openarchivesprotocol.html) to be a valid
[VO Registry](https://ivoa.net/documents/RegistryInterface/20180723/REC-RegistryInterface-1.1.html).

FastAPI usage is limited to main.py (and some type hints and tests) so it
shouldn't be difficult to migrate to Flask or Django if you want to.

This registry is publishing registry with no search features. It is meant to
be harvested by the search registries.

[VOResources](https://www.ivoa.net/documents/latest/VOResource.html)
are stored in static xml files in the `vo_registry/static/resources/
directory. Those files should be the only changes needed to update the registry.
The two required VOResources are included (Registry and Authority).

This registry does not implement Sets or Metadata Formats besides the required
"ivo_managed" set and "ivo_vor"/"oai_dc" metadata formats.

This registry does not implement pagination and any resumption token will
produce an error.

## Updating VOResources

All VOResources are defined in `static/resources/`. YAML, JSON, and XML are
all supported, though XML is preferred for performance.

The resource definition files are validated by the unit tests, which should
be helpful for defining new resources.
