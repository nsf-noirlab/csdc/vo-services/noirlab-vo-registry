terraform {
  backend "http" {
  }
  required_providers {
    google = {
      version = "~> 5.36.0"
      source  = "hashicorp/google"
    }
  }
  required_version = "~> 1.9.0"
}
