## Initial Setup

Before this could be executed in gitlab CI, the service account
and OIDC needed to be configured. So `google_service_account.sa`,
`google_project_iam_member.sa_editor`, and `module.gl_oidc` needed to
be applied locally from an account with sufficient permissions.

There are 4 CI variables that need defining after the initial deploy:
```
SERVICE_ACCOUNT_EMAIL - a terraform output
WI_POOL_PROVIDER - a terraform output
TF_VAR_gitlab_project_id - the gitlab project id (integer)
TF_VAR_google_project_id - the google project id
```

Once the above is configured, the gitlab CI pipeline for plan and apply
should work.

## Executing locally

**NOTE** Enabling the Artifact Registry and Cloud Run APIs in GCP may
take a few minutes and cause the apply to fail. Attempt again after
a few minutes.

0. Change the image input for the two cloud run service resource in main.tf.
The real image doesn't exist yet, but neither does the artifact registry.
So use the hello world image which is commented out to deploy the
infrastructure (and test it), then you can switch it back once you've
deployed the app via the deploy CI jobs.

1. Update variables.tf to set your domain and dev domain.

2. Create `variables_override.tf` and populate it with these values:
```
variable "google_project_id" {
  default = "INSERT GOOGLE PROJECT ID"
}

variable "gitlab_project_id" {
  default = INSERT GITLAB PROJECT ID
}
```

This file is ignored by git.

Please see the Google Cloud provider authentication documentation
to be able to run the initial deployment from your local machine or
a cloud shell:
[Google Cloud provider documentation](https://registry.terraform.io/providers/hashicorp/google/latest/docs/guides/provider_reference#authentication)

I use:
```
gcloud auth application-default login --project {GOOGLE_PROJECT_ID}
```

To initalize directly into the GitLab Terraform state, use this:

```
export GITLAB_USERNAME=<YOUR-USER-NAME>
export GITLAB_ACCESS_TOKEN=<YOUR-ACCESS-TOKEN>
export GITLAB_PROJECT_ID=<PROJECT_ID>

export TF_STATE_NAME=voregistry
terraform init \
    -backend-config="address=https://gitlab.com/api/v4/projects/$GITLAB_PROJECT_ID/terraform/state/$TF_STATE_NAME" \
    -backend-config="lock_address=https://gitlab.com/api/v4/projects/$GITLAB_PROJECT_ID/terraform/state/$TF_STATE_NAME/lock" \
    -backend-config="unlock_address=https://gitlab.com/api/v4/projects/$GITLAB_PROJECT_ID/terraform/state/$TF_STATE_NAME/lock" \
    -backend-config="username=$GITLAB_USERNAME" \
    -backend-config="password=$GITLAB_ACCESS_TOKEN" \
    -backend-config="lock_method=POST" \
    -backend-config="unlock_method=DELETE" \
    -backend-config="retry_wait_min=5"
```

The GitLab Project ID is an integer you can find in `Settings > General`

To apply only the necessary steps from your local machine:

```
terraform apply -target=module.gl_oidc
```
This will enable the necessary APIs, create the service account, and
configure things so that your GitLab CI can use Terraform to manage your GCP
resources (without needing to store keys on GitLab!)

Terraform has two ouputs you should copy into GitLab CI Variables:
oidc_provider which should be the WI_POOL_PROVIDER variable and
service_account_email for the SERVICE_ACCOUNT_EMAIL variable.

**Make sure you select "Expand variable reference"**

You can set these variables as "protected" to only allow protected branches
to access them, but then `terraform plan` won't work on Merge Requests.

### Add Service Account to Google Search Console

Go to https://search.google.com/search-console/users and
add the service account from the Terraform output as a user. This is necessary
to create the domain mapping for the services.

### Use GitLab CI To Provision the Rest

Push your updates to your Repo, create a Merge Request, verify the Terraform plan, merge, and Apply!

## Miscellaneous Notes

### Multiple Applications in the Same GCP Project

By default, this application uses a workload identity provider
and pool with the suffix "vo-registry". If you try to create another identity
provide or pool with the same name, that will cause an error. Using a
different value for `workload_identity_name` will avoid the name collision.

### Destroying Infrastructure

```
Error: Error creating WorkloadIdentityPool: googleapi: Error 409: Requested entity already exists
```

When Terraform destroys the application's infrastructure, it succeeds, but the
Workload Identity Pool has only been soft-deleted, after roughly 30 days it
is fully deleted.

You must update the `workload_identity_name` variable for the `gl_oidc`
module to redeploy to the same project.
