output "oidc_provider" {
  description = "provider"
  value       = "//iam.googleapis.com/${module.gl_oidc.workload_identity_pool_provider_name}"
}

output "service_account_email" {
  description = "service account email"
  value       = google_service_account.sa.email
}
