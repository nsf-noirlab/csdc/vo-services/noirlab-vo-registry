Changelog
=========

All notable changes to this project will be documented in this file.

The format is based on `Keep a Changelog`__

__ https://keepachangelog.com/en/1.0.0/


v0.3.0 (unreleased)
-------------------

Added
~~~~~

- Identify Verb
- ListMetadataFormats Verb
- ListSets Verb
- CI tests for deleted and updated Resources

v0.2.0 (10/16/2023)
-------------------

Added
~~~~~

- versioning


10/16/2023
----------

Changed
~~~~~~~

- changed from Flask to FastAPI

10/08/2023
----------

Added
~~~~~

- E2E tests. Run locally with a new Make target that deploys an e2e test docker
  network. Run in CI using `services:` in gitlab ci. And run against a
  deployed url using Make or Gitlab CI.

10/07/2023
----------

Added
~~~~~

- Gitlab CI now deploys the cloud run services

Removed
~~~~~~~

- Not using Cloud Build anymore

10/03/2023
----------

Added
~~~~~

- Adding Cloud Run with Domain Mapping to terraform

09/30/2023
----------

Added
~~~~~

- Terraform initialized using OIDC

09/23/2023
----------

Added
~~~~~
- GCP Cloud Build config

Changed
~~~~~~~
- Updated Dockerfile to set home location, due to bug with cloud run
  

09/19/2023
----------

Added
~~~~~
- Hello, World! flask app in a docker container with a unit test
- Configured gitlab ci to build, lint, test registry app and
  push containers to the gitlab container registry

09/17/2023
----------

Added
~~~~~
- GitLab Repo Setup: License, Changelog, Readme
